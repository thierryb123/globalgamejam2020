using System;
using UnityEngine;

public class PlayerConstitution : MonoBehaviour
{
    public int MaxFocus = 10;
    public float Focus = 9;

    public int MaxStrength = 8;
    public float Strength = 7;

    public int MaxPatience = 10;
    public float Patience = 9;

    public int MaxEmpathy = 9;
    public float Empathy = 9;

    protected float Hunger = 0;

    protected float Stress = 0;

    public void OnTick()
    {
        Hunger++;
    }

    public void OnInteract(Interactable interactable)
    {
        switch (interactable.InteractableType)
        {
            case InteractableType.FoodSource:
                Hunger = Math.Max(0, Hunger - 3);
                break;
            case InteractableType.Socialize:
                Stress = Math.Max(0, Stress - 1);
                break;
            case InteractableType.Recreation:
                Stress = Math.Max(0, Stress - 2);
                break;
            case InteractableType.Activity:
                break;
        }
    }

    public void OnTaskAdd(TaskList taskList, Task task)
    {
        var workload = Math.Max(0, taskList.Tasks.Count - 1) * 1f;
        Stress += 0.3f * workload;
    }

    public void ApplyActivity(Activity activity)
    {
        Empathy = Math.Max(0, Empathy - activity.Cost.Get(PlayerResource.EMPATHY));
        Focus = Math.Max(0, Focus - activity.Cost.Get(PlayerResource.FOCUS));
        Patience = Math.Max(0, Patience - activity.Cost.Get(PlayerResource.PATIENCE));
        Strength = Math.Max(0, Strength - activity.Cost.Get(PlayerResource.STRENGTH));
    }
}