﻿using UnityEngine;
using System.Linq;

public class NPCSpawner : MonoBehaviour
{
    public GameObject[] Prefabs;

    private void Start()
    {
        var desks = FindObjectsOfType<Interactable>()
            .Where(o => o.InteractableType == InteractableType.Workstation);
            
        foreach (var c in desks)
        {
            Instantiate(Prefabs[UnityEngine.Random.Range(0, Prefabs.Length)], c.transform.position, Quaternion.identity);
        }
    }
}
