﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class PlayerInteract : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            var collider = Physics.OverlapBox(transform.position + Vector3.up * 0.5f, new Vector3(0.5f, 1f, 1f), transform.rotation, LayerMask.GetMask("Interactable"));
            var interactable = collider.Select(c => c.GetComponent<Interactable>()).Where(c => c != null).FirstOrDefault();
            if (interactable != null)
            {
                var toast = FindObjectOfType<Toast>();
                if (interactable.Interact())
                {
                    toast.ShowMessage($"Interacted with {interactable.name}", 10);
                    GetComponent<PlayerConstitution>().OnInteract(interactable);
                }
                else
                {
                    toast.ShowMessage($"Cannot interact with {interactable.name}, try again later", 10);
                }
            }
        }
    }
}
