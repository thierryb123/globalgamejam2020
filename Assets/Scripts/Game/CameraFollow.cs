﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform Track;
    private Vector3 Offset;

    // Start is called before the first frame update
    void Start()
    {
        Offset = Track.position - transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Track.position - Offset;
    }
}
