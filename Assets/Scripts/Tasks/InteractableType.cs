[System.Serializable]
public enum InteractableType
{
    /* Bad */
    Workstation,
    Printer,
    Bin,
    Plant,
    Cabinet,
    Shelf,
    Box,
    CouchTable,
    OfficeDesk,
    ReceptionDesk,
    Coworker,

    /* Good */
    CoffeeMachine,
    Recreation,
    FoodSource,
    Socialize,
    Activity,
}