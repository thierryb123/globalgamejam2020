using System.Collections.Generic;

[System.Serializable]
public class StatusValues
{
    [Newtonsoft.Json.JsonProperty("values")]
    protected Dictionary<PlayerResource, float> values;

    public StatusValues(
        float focus,
        float strength,
        float patience,
        float empathy
    )
    {
        this.values = new Dictionary<PlayerResource, float>();
        this.values.Add(PlayerResource.FOCUS, focus);
        this.values.Add(PlayerResource.STRENGTH, strength);
        this.values.Add(PlayerResource.PATIENCE, patience);
        this.values.Add(PlayerResource.EMPATHY, empathy);
    }

    public float Get(PlayerResource resourceType)
    {
        return this.values[resourceType];
    }

    public StatusValues Set(PlayerResource resourceType, float value)
    {
        this.values[resourceType] = value;
        return this;
    }
}