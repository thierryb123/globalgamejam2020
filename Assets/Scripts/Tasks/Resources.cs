[System.Serializable]
public enum PlayerResource {
    FOCUS,
    STRENGTH,
    PATIENCE,
    EMPATHY,
};

public enum ImpactModifiers {
    HUNGER,
    FATIGUE,
}