﻿using System;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIActivity : MonoBehaviour
{
    private Activity trackedActivity;
    public Activity TrackedActivity
    {
        get
        {
            return trackedActivity;
        }
        set
        {
            trackedActivity = value;
            UpdateContent();
        }
    }

    private TextMeshProUGUI Title;
    private IDictionary<PlayerResource, Image> ResourceIcons = new Dictionary<PlayerResource, Image>();

    private void Awake()
    {
        Title = transform.Find("Title").GetComponent<TextMeshProUGUI>();

        foreach (var n in Enum.GetNames(typeof(PlayerResource)))
        {
            var icon = transform.Find("Resources/" + n)?.GetComponent<Image>();
            PlayerResource r;
            PlayerResource.TryParse(n, out r);
            ResourceIcons[r] = icon;
        }
    }

    private void Update()
    {
        if (trackedActivity == null) return;

        if (trackedActivity.Complete)
        {
            Title.fontStyle = FontStyles.Strikethrough;
        }
        else
        {
            Title.fontStyle = FontStyles.Normal;
        }
    }

    private void UpdateContent()
    {
        Title.text = trackedActivity.Name;
        foreach (PlayerResource r in Enum.GetValues(typeof(PlayerResource)))
        {
            ResourceIcons[r].enabled = (trackedActivity.Cost.Get(r) > 0.0001);
        }
    }
}
