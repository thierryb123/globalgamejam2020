﻿using UnityEngine;

public class UITaskList : MonoBehaviour
{
    public UITask TaskPrefab;

    public void UpdateContent(TaskList taskList, Task task)
    {
        for (int i = transform.childCount - 1; i >= 0; i--)
        {
            DestroyImmediate(transform.GetChild(i).gameObject);
        }
        foreach (var t in taskList.Tasks)
        {
            var obj = Instantiate(TaskPrefab).GetComponent<UITask>();
            obj.transform.SetParent(transform);
            obj.TrackedTask = t;
        }
    }
}
