﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIResources : MonoBehaviour
{
    private PlayerConstitution constitution;
    private IDictionary<PlayerResource, Image> ResourceIcons = new Dictionary<PlayerResource, Image>();

    // Start is called before the first frame update
    void Start()
    {
        constitution = FindObjectOfType<PlayerConstitution>();
        foreach (var n in Enum.GetNames(typeof(PlayerResource)))
        {
            var icon = transform.Find(n)?.GetComponent<Image>();
            PlayerResource r;
            PlayerResource.TryParse(n, out r);
            ResourceIcons[r] = icon;
        }
    }

    // Update is called once per frame
    void Update()
    {
        ResourceIcons[PlayerResource.STRENGTH].fillAmount = constitution.Strength / constitution.MaxStrength;
        ResourceIcons[PlayerResource.EMPATHY].fillAmount = constitution.Empathy / constitution.MaxEmpathy;
        ResourceIcons[PlayerResource.FOCUS].fillAmount = constitution.Focus / constitution.MaxFocus;
        ResourceIcons[PlayerResource.PATIENCE].fillAmount = constitution.Patience / constitution.MaxPatience;
    }
}
